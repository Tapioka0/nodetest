var request = require("supertest");
var app = require("../index.js");
describe("GET /", function () {
  it("respond with hello world", function (done) {
    request(app).get("/").expect('{ "response": "Hello world" }', done);
  });
  it("respond with hello api", function (done) {
    request(app).get("/api").expect('{ "response": "Hello api" }', done);
  });
});
